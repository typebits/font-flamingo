# Flamingo

A wavy typeface with a whimsical attitude.

Made in the [Type:Bits workshop](https://typebits.gitlab.io) in Tomelloso, 2016.

![Font preview](https://gitlab.com/typebits/font-flamingo/-/jobs/artifacts/master/raw/Flamingo-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-flamingo/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-flamingo/-/jobs/artifacts/master/raw/Flamingo-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-flamingo/-/jobs/artifacts/master/raw/Flamingo-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-flamingo/-/jobs/artifacts/master/raw/Flamingo-Regular.sfd?job=build-font)

# Authors

* Guillermo T. Lopez
* Paqui V. Jimenez
* Noelia M. Navarro
* Maria Jose F. Delgado
* Esther A. Jimenez
* Sara M. Brox
* Juan Antonio F. Duran

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
